# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Eduard Selma <selma@tinet.cat>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:51+0200\n"
"PO-Revision-Date: 2021-12-27 14:16+0000\n"
"Last-Translator: Eduard Selma <selma@tinet.cat>, 2021\n"
"Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ca\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: yad-autoremove:12
msgid "Internet connection detected"
msgstr "Detectada connexió a Internet"

#: yad-autoremove:17
msgid "already root"
msgstr "Ja sou 'root'"

#: yad-autoremove:21
msgid "You are Root or running the script in sudo mode"
msgstr "Ja sou 'root' o executeu l'script en mode sudo"

#: yad-autoremove:23
msgid "You entered the wrong password or you cancelled"
msgstr "Heu entrat una contrasenya incorrecta o heu cancel·lat"

#: yad-autoremove:30
msgid "Waiting for a Network connection..."
msgstr "Esperant connexió a Internet..."

#: yad-autoremove:30 yad-autoremove:34
msgid "antiX - autoremove"
msgstr "antiX - autoremove"

#: yad-autoremove:39
msgid "No Internet connection detected!"
msgstr "No s'ha detectat cap connexió a Internet!"
